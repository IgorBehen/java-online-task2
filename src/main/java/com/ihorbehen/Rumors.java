
/**
 * An estimate of the probability that everyone at the party (except Alice) will hear the rumor before it stops
 * propagating, and of the expected number of people to hear
 * the rumor
 * @autor Ihor Behen
 * @version 2.1
 */
package com.ihorbehen;
import java.util.Scanner;


public class Rumors {

    public static void main(String[] args) {
        /**
         * Alice will belong 0
         */

        int numberOfGuests = 0;
        int numberOfInformedGuests = 0;
        int[] informedGuests = null;
        boolean ifGuestKnow = false;

        /**Receiving user input: */
        Scanner scan = new Scanner(System.in);
        System.out.println("Please input the number of guests: ");
        numberOfGuests = scan.nextInt();

        /**Zero check and negative numbers.*/
        if (numberOfGuests >= 1) {

            /**Initializing an array by number of guests.*/
            informedGuests = new int[numberOfGuests];

            /**Alternately we get a list of guests who broadcast the rumor.*/
            for (int i = 0; i < informedGuests.length; i++) {
                int randomGuest = (int) (Math.random() * numberOfGuests + 1);    //*Random numbers from 1 to the number of guests.*/

                /**We check whether the guest hears the second time of the rumor.*/
                for (int j = 0; j < informedGuests.length; j++) {
                    if (informedGuests[j] == randomGuest) {
                        ifGuestKnow = true;
                    }
                }
                /**If the guest has heard this rumor before, we suspend the account of the guests who know.*/
                if (ifGuestKnow) {
                    break;
                } else {           /**If the guest did not hear this rumor, then we add it to the list of guests who have heard only this rumor once*/
                    int[] a = informedGuests;
                    a[i] = randomGuest;
                }

            }

        } else {
            System.out.println("This is the wrong number of guests!");
        }

        scan.close();
        /**Calculating the number of informed guests.*/
        if(informedGuests !=null) {
            for (int k = 0; k < informedGuests.length; k++) {
                if (informedGuests[k] != 0) {
                    numberOfInformedGuests++;
                }
            }
        }

        /**Obtaining results*/
        double everyoneKnowingProbability = (double) numberOfInformedGuests * 100 / numberOfGuests;
        System.out.printf("An estimate of the probability that everyone at the party (except Alice)"
                + " will hear the rumor before it stops propagating: %.2f%%", everyoneKnowingProbability);

        System.out.println();

        System.out.println("An estimate of the expected number of people to hear the rumor: " + numberOfInformedGuests);

    }
}

